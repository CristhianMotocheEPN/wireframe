//
//  CellSettingsTableViewCell.swift
//  Wireframe
//
//  Created by USRDEL on 13/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class CellSettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
