//
//  ViewController.swift
//  Wireframe
//
//  Created by Cristhian Motoche on 3/7/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var textDescription: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.textDescription.text = "Shortly, you will be sent an email confirming all the information you have entered. If you have any questions please contact 0 123 456 789 or if you'd like to make any changes to your details go to Settings within the app."
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func salir(segue: UIStoryboardSegue) {
    }
}
